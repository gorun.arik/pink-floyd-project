import socket
import questions_library
LISTEN_PORT = 666
WELCOME = "Welcome to the Pink Floyd server!"
FILE = r"C:\Users\magshimim\Documents\מגשימים_תרגול\Pink_Floyd_DB.txt"

def get_data(data):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    cmd = data[4]
    real_data = data[11:]
    return cmd, real_data

def data_structure():
    with open(FILE, "r") as to_read:
        read = to_read.read()
    album_array = read.split("#")
    general_dict = {}
    for i in range(1, len(album_array)):
        album = album_array[i].split("*")[0][:-1]
        songs = album_array[i].split("*")[1:]
        internal_dict = {}
        for song in songs:
            song_start = song.find("::", 2+song.find("::", 2+song.find("::")))
            internal_dict[song[:song_start]] = song[song_start + 2:]
        general_dict[album] = internal_dict
    return general_dict

def main():
    #call the function find_checksum
    connected = False
    structure = data_structure()
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while not connected:
        try:
            server_address = ('', LISTEN_PORT)
            listening_sock.bind(server_address)
            connected = True
        except Exception as e:
            print("Error:\n", e, "\nPort is not free! trying again")

    listening_sock.listen(1)
    while True:
        cmd = '0'
        try:
            client_soc, client_address = listening_sock.accept()
            client_soc.sendall(WELCOME.encode())
            while not int(cmd) == 8:
                client_msg = client_soc.recv(1024)
                client_msg = client_msg.decode()
                if (not client_msg[:4] == "cmd:") or (not '0' <= client_msg[4] <= '9') or (not client_msg[5:11] == "&data:"):
                    msg = "ERROR:Incorrect format"
                    client_soc.sendall(msg.encode())
                    continue
                cmd, data = get_data(client_msg)
                if int(cmd) == 0:
                    msg = questions_library.list_of_longest_albums(structure)
                elif int(cmd) == 1:
                    msg = questions_library.albums_list(structure)
                elif int(cmd) == 2:
                    msg = questions_library.songs_list_in_album(data, structure)
                elif int(cmd) == 3:
                    msg = questions_library.length_of_song(data, structure)
                elif int(cmd) == 4:
                    msg = questions_library.words_in_song(data, structure)
                elif int(cmd) == 5:
                    album, song = questions_library.album_of_song(data, structure)
                    msg = song + " is in the " + album[:album.find("::")] + " album"
                elif int(cmd) == 6:
                    msg = questions_library.song_names_with_word(data, structure)
                elif int(cmd) == 7:
                    msg = questions_library.word_in_songs(data, structure)
                elif int(cmd) == 8:
                    msg = "GoodBye!"
                elif int(cmd) == 9:
                    msg = questions_library.most_common_words(structure)
                client_soc.sendall(msg.encode())
        except Exception as e:
            print(e, "\n")
        client_soc.close()
    listening_sock.close()
    
if __name__ == "__main__":
    main()