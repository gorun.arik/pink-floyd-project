import socket
SERVER_IP = "77.125.242.167"
SERVER_PORT = 666
MENU = "Choose one option:\n0 - Show list of longest albums\n1 - Show all albums\n2 - Show all songs in album\n3 - Show length of a song\n4 - Show the words of the song\n5 - Show album of song\n6 - Show songs by part of their name\n7 - Show songs by part of their lyrics\n8 - Quit\n9 - Show 50 most common words\n"

def create_format(cmd, data):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    format = "cmd:" + cmd + "&data:" + data
    return format
    
def main():
    #call the function find_checksum
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    msg = ""
    cmd = '0'
    print(server_msg)
    while not int(cmd) == 8:
        cmd = input("\n" + MENU)
        if int(cmd) == 2:
            data = input("Enter album name: ")
        elif int(cmd) == 3:
            data = input("Enter song name: ")
        elif int(cmd) == 4:
            data = input("Enter song name: ")
        elif int(cmd) == 5:
            data = input("Enter song name: ")
        elif int(cmd) == 6:
            data = input("Enter a word: ")
        elif int(cmd) == 7:
            data = input("Enter a lyric: ")
        else:
            data = ""
        msg = create_format(cmd, data)
        sock.sendall(msg.encode())
        server_msg = sock.recv(2048)
        server_msg = server_msg.decode()
        print(server_msg)
    sock.close()
    
if __name__ == "__main__":
    main()