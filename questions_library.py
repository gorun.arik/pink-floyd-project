import re
def albums_list(data_struct):
    """The function gives the list of albums
	:param data_struct: the structure
    :type data_struct: dict
    :return: ans
    :rtype: str
    """
    albums = []
    for album_name in data_struct.keys():
        albums += [album_name[:album_name.find("::")]]
    ans = "These are the albums:\n" + '\n'.join(albums)
    return ans
    
def songs_list_in_album(album, data_struct):
    """The function gives all the songs in the album
    :param album: The album name
	:param data_struct: the structure
    :type data_struct: dict
	:type album: str
    :return: ans
    :rtype: str
    """
    for album_name in data_struct.keys():
        if album in album_name:
            album = album_name
    songs = []
    for song_name in data_struct[album].keys():
        songs += [song_name[:song_name.find("::")]]
    ans = "These are the songs in " + album[:album.find("::")] + ":\n" + '\n'.join(songs)
    return ans
    
def length_of_song(song, data_struct):
    """The function gives the length of a song
    :param song: The song name
	:param data_struct: the structure
    :type data_struct: dict
	:type song: str
    :return: ans
    :rtype: str
    """
    album_name, real_song = album_of_song(song, data_struct)
    for song_name in data_struct[album_name].keys():
        if real_song in song_name:
            real_song = song_name
    length = real_song[2+real_song.find("::", 2+real_song.find("::")):]
    ans = "The length of " + song + " is " + length
    return ans
    
def words_in_song(song, data_struct):
    """The function gives the lyrics of the song
    :param song: The song name
	:param data_struct: the structure
    :type data_struct: dict
	:type song: str
    :return: ans
    :rtype: str
    """
    album_name, real_song = album_of_song(song, data_struct)
    for song_name in data_struct[album_name].keys():
        if real_song in song_name:
            real_song = song_name
    lyrics = data_struct[album_name][real_song]
    ans = "These are the lyrics of " + song + ":\n" + lyrics
    return ans
    
def album_of_song(song, data_struct):
    """The function gives the album in which the song is in
    :param song: The song name
	:param data_struct: the structure
    :type data_struct: dict
	:type song: str
    :return: ans
    :rtype: str
    """
    real_album = ""
    for album_name in data_struct.keys():
        for song_name in data_struct[album_name].keys():
            if song == song_name[:song_name.find("::")]:
                song = song_name
                real_album = album_name
    return real_album, song[:song.find("::")]
    
def song_names_with_word(word, data_struct):
    """The function gives the songs which have the word in their name
    :param word: a word to search
	:param data_struct: the structure
    :type data_struct: dict
	:type word: str
    :return: ans
    :rtype: str
    """
    songs = []
    for album_name in data_struct.keys():
        for song_name in data_struct[album_name].keys():
            if word.lower() in song_name[:song_name.find("::")].lower():
                songs += [song_name[:song_name.find("::")]]
    ans = "These are the songs which have \"" + word + "\" in their name:\n" + '\n'.join(songs)
    return ans
    
def word_in_songs(word, data_struct):
    """The function gives the songs in which the word is in the lyrics
    :param word: a word to search
	:param data_struct: the structure
    :type data_struct: dict
	:type word: str
    :return: ans
    :rtype: str
    """
    songs = []
    for album_name in data_struct.keys():
        for song_name in data_struct[album_name].keys():
            if word.lower() in data_struct[album_name][song_name].lower():
                songs += [song_name[:song_name.find("::")]]
    ans = "These are the songs which have \"" + word + "\" in their lyrics:\n" + '\n'.join(songs)  
    return ans
    
def most_common_words(data_struct):
    """The function gives the 50 most common words in all of the songs and the amount of them
    :param data_struct: the structure
    :type data_struct: dict
    :return: ans
    :rtype: str
    """
    common_dict = {}
    for album_name in data_struct.keys():
        for song in data_struct[album_name].values():
            words = re.split('\s|\?|\!',song)
            for word in words:
                if word in common_dict.keys():
                    common_dict[word] += 1
                else:
                    common_dict[word] = 1
    list = []
    common_list = sorted(common_dict, key=common_dict.__getitem__, reverse=True)
    for i in range(50):
        list.append(str(i+1) + ". " + common_list[i] + " - " + str(common_dict[common_list[i]]) + " times")
    ans = "These are the most common words in all Pink Floyd songs:\n" + '\n'.join(list)  
    return ans
    
def list_of_longest_albums(data_struct):
    """The function gives the total length of each album from highest to lowest
    :param data_struct: the structure
    :type data_struct: dict
    :return: ans
    :rtype: str
    """
    length_dict = {}
    for album_name in data_struct.keys():
        for song_name in data_struct[album_name].keys():
            lst = song_name.split("::")
            if album_name in length_dict:
                length_dict[album_name] += int(lst[2].split(":")[0])*60 + int(lst[2].split(":")[1])
            else:
                length_dict[album_name] = 0
    length_list = sorted(length_dict, key=length_dict.__getitem__, reverse=True)
    list = []
    for i in range(len(length_list)):
        list.append(str(i+1) + ". " + length_list[i] + " - " + str((length_dict[length_list[i]])//60) + ":" + str((length_dict[length_list[i]])%60) + " minutes")
    ans = "These are the albums sorted from longest to shortest:\n" + '\n'.join(list)  
    return ans