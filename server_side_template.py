import socket
LISTEN_PORT = 666
WELCOME = "Welcome to the Pink Floyd server!"
REQUESTS = {1:"These are the albums", 2:"Here are the songs in this album", 3:"This is the length of this song", 4:"Here are the words of this song",
 5:"This song is in this album",6:"This word is in these song names", 7:"This word exists in these songs"}
FILE = r"C:\Users\magshimim\Documents\מגשימים_תרגול\Pink_Floyd_DB.txt"


def get_data(data):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    cmd = data[4]
    real_data = data[11:]
    return cmd, real_data
    
def albums_list():
    """The function finds th weather app checksum
    :return: the checksum
    :rtype: str
    """
    return REQUESTS[1]
    
def songs_list_in_album(album):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    return_value = REQUESTS[2][:REQUESTS[2].find("this")] + album
    return return_value
    
def length_of_song(song):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    return_value = REQUESTS[3][:REQUESTS[3].find("this")] + song
    return return_value
    
def words_in_song(song):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    return_value = REQUESTS[4][:REQUESTS[4].find("this")] + song
    return return_value
    
def album_of_song(song):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    return_value = song + REQUESTS[5][9:]
    return return_value
    
def song_names_with_word(word):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    return_value = word + REQUESTS[6][9:]
    return return_value
    
def word_in_songs(word):
    """The function finds th weather app checksum
    :param city: the city 
    :type date: str
    :return: the checksum
    :rtype: str
    """
    return_value = word + REQUESTS[7][9:]
    return return_value
    
def main():
    #call the function find_checksum
    connected = False
    listening_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while not connected:
        try:
            server_address = ('', LISTEN_PORT)
            listening_sock.bind(server_address)
            connected = True
        except Exception as e:
            print("Error:\n", e, "\nPort is not free! trying again")

    listening_sock.listen(1)
    while True:
        cmd = '0'
        try:
            client_soc, client_address = listening_sock.accept()
            client_soc.sendall(WELCOME.encode())
            while not int(cmd) == 8:
                client_msg = client_soc.recv(1024)
                client_msg = client_msg.decode()
                if (not client_msg[:4] == "cmd:") or (not '1' <= client_msg[4] <= '8') or (not client_msg[5:11] == "&data:"):
                    msg = "ERROR:Incorrect format"
                    client_soc.sendall(msg.encode())
                    continue
                cmd, data = get_data(client_msg)
                if int(cmd) == 1:
                    msg = albums_list()
                elif int(cmd) == 2:
                    msg = songs_list_in_album(data)
                elif int(cmd) == 3:
                    msg = length_of_song(data)
                elif int(cmd) == 4:
                    msg = words_in_song(data)
                elif int(cmd) == 5:
                    msg = album_of_song(data)
                elif int(cmd) == 6:
                    msg = song_names_with_word(data)
                elif int(cmd) == 7:
                    msg = word_in_songs(data)
                elif int(cmd) == 8:
                    msg = "GoodBye!"
                client_soc.sendall(msg.encode())
        except Exception as e:
            print("The client has exited forcibly.\n", e, "\n")
        client_soc.close()
    listening_sock.close()
    
if __name__ == "__main__":
    main()